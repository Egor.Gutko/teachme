package com.example.myapplication.di

import com.example.myapplication.fragments.books.BookViewModel
import com.example.myapplication.fragments.profile.ProfileViewModel
import com.example.myapplication.fragments.profile.age.ProfileAgeViewModel
import com.example.myapplication.fragments.profile.name.ProfileNameViewModel
import com.example.myapplication.fragments.profile.number.ProfileNumberViewModel
import com.example.myapplication.fragments.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<RegistrationViewModel> {
        RegistrationViewModel(get())
    }

    viewModel<ProfileViewModel> {
        ProfileViewModel(get())
    }

    viewModel<ProfileAgeViewModel> {
        ProfileAgeViewModel(get())
    }

    viewModel<ProfileNameViewModel> {
        ProfileNameViewModel(get())
    }

    viewModel<ProfileNumberViewModel> {
        ProfileNumberViewModel(get())
    }

    viewModel<BookViewModel> {
        BookViewModel(get())
    }
}