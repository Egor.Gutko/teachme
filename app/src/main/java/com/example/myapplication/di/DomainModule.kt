package com.example.myapplication.di

import com.example.domain.usecase.*
import org.koin.dsl.module

val domainModule = module {

    factory<EditAgeUseCase> { EditAgeUseCase(get()) }

    factory<EditNumberUseCase> { EditNumberUseCase(get()) }

    factory<EditNameUseCase> { EditNameUseCase(get()) }

    factory<GetUserInformationUseCase> { GetUserInformationUseCase(get()) }

    factory<LoginUseCase> { LoginUseCase(get()) }

    factory<RegistrationUseCase> { RegistrationUseCase(get()) }

    factory<GetBooksUseCase> { GetBooksUseCase(get()) }
}