package com.example.myapplication.di

import androidx.room.Room
import com.example.data.RetrofitCreator
import com.example.data.converters.BookEntityToBookDtoConverter
import com.example.data.converters.BookToBookDtoConverter
import com.example.data.converters.BookToBookEntityConverter
import com.example.data.repo.AuthRepositoryImpl
import com.example.data.repo.NetworkRepositoryImpl
import com.example.data.repo.UserProfileRepositoryImpl
import com.example.data.storage.auth.AuthStorage
import com.example.data.storage.auth.AuthStorageSharedPrefImpl
import com.example.data.storage.database.BookDataBase
import com.example.data.storage.database.DataBaseBookStorage
import com.example.data.storage.database.DataBaseBookStorageImpl
import com.example.data.storage.network.NetworkService
import com.example.data.storage.network.NetworkStorage
import com.example.data.storage.network.NetworkStorageImpl
import com.example.data.storage.profile.ProfileStorage
import com.example.data.storage.profile.ProfileStoragePrefImpl
import com.example.domain.repository.AuthRepository
import com.example.domain.repository.NetworkRepository
import com.example.domain.repository.UserProfileRepository
import com.google.gson.GsonBuilder
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val dataModule = module {

    single { GsonBuilder().serializeNulls().create() }

    val creator = RetrofitCreator()

    single { creator.createService(get(), NetworkService::class.java) as NetworkService }

    single {
        Room.databaseBuilder(androidApplication(), BookDataBase::class.java, "book_data_base")
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<BookDataBase>().bookDao() }

    single<AuthStorage> {
        AuthStorageSharedPrefImpl(get())
    }

    single<ProfileStorage> {
        ProfileStoragePrefImpl(get())
    }

    single<NetworkStorage> {
        NetworkStorageImpl(get())
    }

    single<DataBaseBookStorage> { DataBaseBookStorageImpl(get()) }

    single<AuthRepository> {
        AuthRepositoryImpl(get())
    }

    single<UserProfileRepository> {
        UserProfileRepositoryImpl(get())
    }

    single<NetworkRepository> {
        NetworkRepositoryImpl(get(), get(), get(), get(), get())
    }

    factory { BookToBookDtoConverter() }

    factory { BookEntityToBookDtoConverter() }

    factory { BookToBookEntityConverter() }
}