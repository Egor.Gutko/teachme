package com.example.myapplication

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.example.myapplication.databinding.CustomViewBinding

class CustomView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = CustomViewBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        with(binding) {
            context.obtainStyledAttributes(attrs, R.styleable.CustomView).use { typedArray ->
                when (typedArray.getInt(R.styleable.CustomView_visibleItem, 0)) {
                    0 -> {
                        imageFirst.isVisible = true
                        imageSecond.isVisible = false
                    }
                    1 -> {
                        imageFirst.isVisible = false
                        imageSecond.isVisible = true
                    }
                    else -> {
                        imageFirst.isVisible = true
                        imageSecond.isVisible = true
                    }
                }
            }
        }
    }

    fun setOnFirstListener(action: () -> Unit) {
        binding.imageFirst.setOnClickListener {
            action.invoke()
        }
    }

}