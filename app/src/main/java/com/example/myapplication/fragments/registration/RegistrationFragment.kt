package com.example.myapplication.fragments.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.domain.models.RegistrationUserModel
import com.example.myapplication.databinding.FragmentRegistrationBinding
import com.example.myapplication.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegistrationFragment : BaseFragment<FragmentRegistrationBinding>() {

    private val viewModel by viewModel<RegistrationViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentRegistrationBinding = FragmentRegistrationBinding.inflate(inflater, container, false)

    override fun FragmentRegistrationBinding.onBindView(saveInstanceState: Bundle?) {
        submit.setOnClickListener {
            val registrationUserModel = RegistrationUserModel(
                login = addName.text.toString(),
                password = enterPassword.text.toString(),
                repeatPassword = repeatPassword.text.toString()
            )
            viewModel.registration(registrationUserModel)
        }

        viewModel.registrationLiveData.observe(viewLifecycleOwner) { result ->
            if (result) {
                navController.navigate(
                    RegistrationFragmentDirections.navigateToMainFragment()
                )
            } else {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_LONG).show()
            }
        }
    }
}