package com.example.myapplication.fragments.profile.age

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.databinding.FragmentProfileAgeBinding
import com.example.myapplication.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileAgeFragment : BaseFragment<FragmentProfileAgeBinding>() {

    private val viewModel by viewModel<ProfileAgeViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentProfileAgeBinding = FragmentProfileAgeBinding.inflate(inflater, container, false)

    override fun FragmentProfileAgeBinding.onBindView(saveInstanceState: Bundle?) {
        numberPicker.minValue = 12
        numberPicker.maxValue = 100
        numberPicker.wrapSelectorWheel = true

        submit.setOnClickListener {
            viewModel.editAge(numberPicker.value)
            navController.popBackStack()
        }
    }
}