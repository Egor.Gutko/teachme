package com.example.myapplication.fragments.profile.name

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditNameUseCase

class ProfileNameViewModel(private val editNameUseCase: EditNameUseCase) : ViewModel() {

    fun editName(name: String) {
        editNameUseCase.execute(name)
    }
}