package com.example.myapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.databinding.FragmentFirstBinding
import com.example.myapplication.fragments.base.BaseFragment

class FirstFragment : BaseFragment<FragmentFirstBinding>() {

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFirstBinding = FragmentFirstBinding.inflate(inflater, container, false)

    override fun FragmentFirstBinding.onBindView(saveInstanceState: Bundle?) {
        login.setOnClickListener {
            navController.navigate(FirstFragmentDirections.navigateToLoginFragment())
        }
        registration.setOnClickListener {
            navController.navigate(FirstFragmentDirections.navigateToRegistrationFragment())
        }
        firstMyView.setOnFirstListener {
            println("___________________________________")
        }
    }
}