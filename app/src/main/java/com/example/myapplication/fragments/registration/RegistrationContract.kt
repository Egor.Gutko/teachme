package com.example.myapplication.fragments.registration

import com.example.domain.models.RegistrationUserModel

interface RegistrationContract {

    interface RegistrationPresenterContract {

        fun attachView(registrationFragment: RegistrationFragmentContract)

        fun detachView()

        fun registration(model: RegistrationUserModel)
    }

    interface RegistrationFragmentContract {

        fun registrationSuccess(login: String)

        fun registrationFailed()
    }
}