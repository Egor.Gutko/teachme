package com.example.myapplication.fragments.books

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.models.BookDto
import com.example.domain.usecase.GetBooksUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class BookViewModel(private val bookUseCase: GetBooksUseCase) : ViewModel() {

    private val _state: MutableStateFlow<List<BookDto>> = MutableStateFlow(emptyList())
    val state: StateFlow<List<BookDto>> = _state

    fun getBooks(count: Int) {
        viewModelScope.launch {
            _state.emit(bookUseCase.execute(count))
        }
    }
}