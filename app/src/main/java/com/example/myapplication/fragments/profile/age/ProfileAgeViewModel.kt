package com.example.myapplication.fragments.profile.age

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditAgeUseCase

class ProfileAgeViewModel(private val editAgeUseCase: EditAgeUseCase) : ViewModel() {

    fun editAge(age: Int) {
        editAgeUseCase.execute(age)
    }
}