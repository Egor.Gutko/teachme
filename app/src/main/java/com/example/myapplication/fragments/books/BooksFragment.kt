package com.example.myapplication.fragments.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.myapplication.databinding.FragmentBooksBinding
import com.example.myapplication.fragments.base.BaseFragment
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class BooksFragment : BaseFragment<FragmentBooksBinding>() {

    private val viewModel by viewModel<BookViewModel>()

    private val bookAdapter = BookAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentBooksBinding = FragmentBooksBinding.inflate(inflater, container, false)

    override fun FragmentBooksBinding.onBindView(saveInstanceState: Bundle?) {
        viewModel.getBooks(10)
        bookList.adapter = bookAdapter

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.state.collect{list->
                    bookAdapter.submitList(list)
                }
            }
        }
    }
}