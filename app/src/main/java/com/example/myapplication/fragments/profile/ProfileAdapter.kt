package com.example.myapplication.fragments.profile

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.domain.models.ProfileDivider
import com.example.domain.models.ProfileItem
import com.example.myapplication.databinding.ProfileDividerBinding
import com.example.myapplication.databinding.ProfileItemBinding
import com.example.myapplication.fragments.base.BaseViewHolder

class ProfileAdapter :
    ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is ProfileItem && newItem is ProfileItem -> oldItem.title == newItem.title
            oldItem is ProfileDivider && newItem is ProfileDivider -> true
            else -> false
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is ProfileItem && newItem is ProfileItem -> oldItem == newItem
            oldItem is ProfileDivider && newItem is ProfileDivider -> true
            else -> false
        }
    }) {

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is ProfileItem -> PROFILE_ITEM_TYPE
        is ProfileDivider -> PROFILE_DIVIDER_TYPE
        else -> throw IllegalArgumentException("ProfileAdapter can't handle item" + getItem(position))
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        PROFILE_ITEM_TYPE -> ProfileViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        PROFILE_DIVIDER_TYPE -> ProfileDividerViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("ProfileAdapter can't handle $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val PROFILE_ITEM_TYPE = 999
        private const val PROFILE_DIVIDER_TYPE = 998
    }
}

private class ProfileViewHolder(parent: ViewGroup) :
    BaseViewHolder<ProfileItemBinding, ProfileItem>(
        ProfileItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun ProfileItemBinding.bind(value: ProfileItem) {
        title.text = value.title
        subTitle.text = value.subtitle
        itemView.setOnClickListener {
            value.action()
        }
    }
}

private class ProfileDividerViewHolder(parent: ViewGroup) :
    BaseViewHolder<ProfileDividerBinding, ProfileDivider>(
        ProfileDividerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )