package com.example.myapplication.fragments.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.domain.models.ProfileDivider
import com.example.domain.models.ProfileItem
import com.example.myapplication.DialogWithTwoButtons
import com.example.myapplication.databinding.FragmentProfileBinding
import com.example.myapplication.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val adapter = ProfileAdapter()

    private val viewModel by viewModel<ProfileViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentProfileBinding = FragmentProfileBinding.inflate(inflater, container, false)

    override fun FragmentProfileBinding.onBindView(saveInstanceState: Bundle?) {
        back.setOnClickListener {
            val requester = setResultListenerForDialogWithTwoButtons()
            navController.navigate(ProfileFragmentDirections.navigateToTwoButtonDialog(requester))
        }
        items.adapter = adapter
        viewModel.getUserInfo()
        viewModel.userInfoLiveData.observe(viewLifecycleOwner) { userProfile ->
            adapter.submitList(
                listOf(
                    ProfileDivider,
                    ProfileItem("number", userProfile.number) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileNumberFragment())
                    },
                    ProfileDivider,
                    ProfileItem("name", userProfile.name) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileNameFragment())
                    },
                    ProfileDivider,
                    ProfileItem("age", userProfile.age.toString()) {
                        navController.navigate(ProfileFragmentDirections.actionProfileFragmentToProfileAgeFragment())
                    },
                    ProfileDivider,
                    ProfileItem("email", userProfile.email) {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("https://www.google.com/")
                        startActivity(intent)
                    },
                    ProfileDivider
                )
            )
        }
    }

    private fun setResultListenerForDialogWithTwoButtons() =
        setResultListenerWithCheckRequester(DialogWithTwoButtons.REQUEST_KEY) {
            if (it.getBoolean(DialogWithTwoButtons.RESULT_KEY)) {
                navController.popBackStack()
            } else {
                Toast.makeText(requireContext(), "No", Toast.LENGTH_LONG).show()
            }
        }
}