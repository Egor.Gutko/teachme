package com.example.myapplication.fragments.profile.number

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditNumberUseCase

class ProfileNumberViewModel(private val editNumberUseCase: EditNumberUseCase) : ViewModel() {

    fun editNumber(number: String) {
        editNumberUseCase.execute(number)
    }
}