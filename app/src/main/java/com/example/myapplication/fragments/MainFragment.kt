package com.example.myapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.databinding.FragmentMainBinding
import com.example.myapplication.fragments.base.BaseFragment

class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)

    override fun FragmentMainBinding.onBindView(saveInstanceState: Bundle?) {
        name.text = "Hello"
        name.setOnClickListener {
            navController.navigate(MainFragmentDirections.navigateToProfileFragment())
        }
    }
}