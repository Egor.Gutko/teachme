package com.example.myapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.data.repo.AuthRepositoryImpl
import com.example.data.storage.auth.AuthStorage
import com.example.data.storage.auth.AuthStorageSharedPrefImpl
import com.example.domain.models.UserCredo
import com.example.domain.usecase.LoginUseCase
import com.example.myapplication.databinding.FragmentLoginBinding
import com.example.myapplication.fragments.base.BaseFragment

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val loginUseCase by lazy { LoginUseCase(authRepository) }
    private val authRepository by lazy { AuthRepositoryImpl(authStorage) }
    private val authStorage: AuthStorage by lazy { AuthStorageSharedPrefImpl(requireContext()) }

    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentLoginBinding = FragmentLoginBinding.inflate(inflater, container, false)

    override fun FragmentLoginBinding.onBindView(saveInstanceState: Bundle?) {
        submit.setOnClickListener {
            val userCredo = UserCredo(
                login = addName.text.toString(), password = enterPassword.text.toString()
            )
            if (loginUseCase.execute(userCredo)) {
                navController.navigate(LoginFragmentDirections.navigateToMainFragment())
            } else {
                Toast.makeText(requireContext(), "Enter Credo", Toast.LENGTH_SHORT).show()
            }
        }
    }
}