package com.example.myapplication.fragments.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.models.RegistrationUserModel
import com.example.domain.usecase.RegistrationUseCase

class RegistrationViewModel(private val registrationUseCase: RegistrationUseCase) : ViewModel() {

    private val _registrationLiveData = MutableLiveData<Boolean>()
    val registrationLiveData: LiveData<Boolean> = _registrationLiveData

    fun registration(model: RegistrationUserModel) {
        val result = registrationUseCase.execute(param = model)
        _registrationLiveData.value = result
    }
}