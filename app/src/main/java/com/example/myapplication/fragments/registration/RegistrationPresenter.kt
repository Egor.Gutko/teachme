package com.example.myapplication.fragments.registration

import com.example.domain.models.RegistrationUserModel
import com.example.domain.usecase.RegistrationUseCase

class RegistrationPresenter(
    private val registrationUseCase: RegistrationUseCase
) : RegistrationContract.RegistrationPresenterContract {
    private var view: RegistrationContract.RegistrationFragmentContract? = null

    override fun attachView(registrationFragment: RegistrationContract.RegistrationFragmentContract) {
        view = registrationFragment
    }

    override fun detachView() {
        view = null
    }

    override fun registration(model: RegistrationUserModel) {
        val result = registrationUseCase.execute(model)
        if (result) {
            view?.registrationSuccess(login = model.login)
        } else {
            view?.registrationFailed()
        }
    }
}