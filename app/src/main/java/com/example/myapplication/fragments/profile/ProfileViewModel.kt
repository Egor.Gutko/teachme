package com.example.myapplication.fragments.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.models.UserProfileInfo
import com.example.domain.usecase.GetUserInformationUseCase

class ProfileViewModel(private val getUserInformationUseCase: GetUserInformationUseCase) :
    ViewModel() {

    init {
        println("______________________________________________________")
    }

    private val _userInfoLiveData = MutableLiveData<UserProfileInfo>()
    val userInfoLiveData: LiveData<UserProfileInfo> = _userInfoLiveData

    fun getUserInfo() {
        val result = getUserInformationUseCase.execute()
        _userInfoLiveData.value = result
    }
}