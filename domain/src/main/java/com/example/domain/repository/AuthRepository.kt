package com.example.domain.repository

import com.example.domain.models.RegistrationUserModel
import com.example.domain.models.UserCredo

interface AuthRepository {

    fun checkUserCredo(userCredo: UserCredo): Boolean

    fun registerUser(model: RegistrationUserModel): Boolean
}