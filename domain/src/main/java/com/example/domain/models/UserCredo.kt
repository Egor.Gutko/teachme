package com.example.domain.models

data class UserCredo(
    val login: String,
    val password: String
)