package com.example.domain.models

data class RegistrationUserModel(
    val login: String,
    val password: String,
    val repeatPassword: String
)