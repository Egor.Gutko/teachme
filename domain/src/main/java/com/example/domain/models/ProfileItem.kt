package com.example.domain.models

data class ProfileItem(
    val title: String,
    val subtitle: String,
    val action: ()->Unit
)
