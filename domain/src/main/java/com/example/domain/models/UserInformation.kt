package com.example.domain.models

data class UserInformation(
    val userName: String,
    val age: Int
)