package com.example.domain.models

data class UserProfileInfo(
    val number: String,
    val name: String,
    val age: Int,
    val email: String,
)
