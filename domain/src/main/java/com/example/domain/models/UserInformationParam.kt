package com.example.domain.models

data class UserInformationParam(
    val name: String
)