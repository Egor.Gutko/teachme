package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserProfileRepository

class EditAgeUseCase(private val profileRepository: UserProfileRepository) : UseCase<Int, Unit> {
    override fun execute(param: Int?) {
        profileRepository.editProfileAge(param!!)
    }
}