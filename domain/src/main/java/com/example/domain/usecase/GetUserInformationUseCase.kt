package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.models.UserProfileInfo
import com.example.domain.repository.UserProfileRepository

class GetUserInformationUseCase(private val userProfileRepository: UserProfileRepository) :
    UseCase<Unit, UserProfileInfo> {
    override fun execute(param: Unit?): UserProfileInfo {
        return userProfileRepository.getUserProfileInfo()
    }
}