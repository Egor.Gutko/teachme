package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserProfileRepository

class EditNameUseCase(private val userProfileRepository: UserProfileRepository) :
    UseCase<String, Unit> {
    override fun execute(param: String?) {
        return userProfileRepository.editProfileName(param!!)
    }
}