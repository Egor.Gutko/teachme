package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.models.RegistrationUserModel
import com.example.domain.repository.AuthRepository

class RegistrationUseCase(private val authRepository: AuthRepository) :
    UseCase<RegistrationUserModel, Boolean> {
    override fun execute(param: RegistrationUserModel?): Boolean {
        return authRepository.registerUser(param!!)
    }
}