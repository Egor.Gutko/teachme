package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.models.UserCredo
import com.example.domain.repository.AuthRepository

class LoginUseCase(private val authRepository: AuthRepository) : UseCase<UserCredo, Boolean> {
    override fun execute(param: UserCredo?): Boolean {
        return authRepository.checkUserCredo(param!!)
    }
}