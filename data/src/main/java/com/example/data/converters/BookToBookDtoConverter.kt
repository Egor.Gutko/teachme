package com.example.data.converters

import com.example.data.storage.network.model.Book
import com.example.domain.base.Converter
import com.example.domain.models.BookDto

class BookToBookDtoConverter : Converter<Book, BookDto> {

    override fun invoke(params: Book): BookDto {
        return BookDto(
            params.id ?: -1,
            params.title ?: "",
            params.author ?: "",
            params.genre ?: "",
            params.description ?: "",
            params.isbn ?: "",
            params.image ?: "",
            params.published ?: "",
            params.publisher ?: ""
        )
    }
}