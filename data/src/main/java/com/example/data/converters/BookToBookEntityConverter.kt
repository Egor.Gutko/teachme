package com.example.data.converters

import com.example.data.storage.database.BookEntity
import com.example.data.storage.network.model.Book
import com.example.domain.base.Converter

class BookToBookEntityConverter : Converter<Book, BookEntity> {
    override fun invoke(params: Book): BookEntity {
        return BookEntity(
            id = params.id,
            title = params.title,
            author = params.author,
            genre = params.genre,
            description = params.description,
            isbn = params.isbn,
            image = params.image,
            publisher = params.publisher,
            published = params.published
        )
    }
}