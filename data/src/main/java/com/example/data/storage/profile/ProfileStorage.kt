package com.example.data.storage.profile

import com.example.domain.models.UserProfileInfo

interface ProfileStorage {
    fun getProfileInfo(): UserProfileInfo

    fun editProfileNumber(number: String)

    fun editProfileName(name: String)

    fun editProfileAge(age: Int)
}