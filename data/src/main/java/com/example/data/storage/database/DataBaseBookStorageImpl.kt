package com.example.data.storage.database

class DataBaseBookStorageImpl(
    private val bookDao: BookDao
) : DataBaseBookStorage {

    override suspend fun getBooks(): List<BookEntity> = bookDao.getAllBooks()

    override suspend fun insertBook(bookEntity: BookEntity) = bookDao.insertAllBooks(bookEntity)
}