package com.example.data.storage.auth

import com.example.domain.models.UserCredo

interface AuthStorage {

    fun saveCredo(userCredo: UserCredo)

    fun getCredo(): UserCredo
}