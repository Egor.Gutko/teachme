package com.example.data.storage.auth

import com.example.domain.models.UserCredo

object AuthStorageSimple : AuthStorage {

    private var login: String = ""
    private var password: String = ""

    override fun saveCredo(userCredo: UserCredo) {
        login = userCredo.login
        password = userCredo.password
    }

    override fun getCredo(): UserCredo {
        return UserCredo(login, password)
    }
}