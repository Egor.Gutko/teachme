package com.example.data.storage.network.model

import com.google.gson.annotations.SerializedName

data class Book(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("author") val author: String? = null,
    @SerializedName("genre") val genre: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("isbn") val isbn: String? = null,
    @SerializedName("image") val image: String? = null,
    @SerializedName("published") val published: String? = null,
    @SerializedName("publisher") val publisher: String? = null
)
