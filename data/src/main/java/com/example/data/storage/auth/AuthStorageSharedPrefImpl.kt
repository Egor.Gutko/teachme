package com.example.data.storage.auth

import android.content.Context
import androidx.core.content.edit
import com.example.domain.models.UserCredo

class AuthStorageSharedPrefImpl(context: Context) : AuthStorage {

    private val sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)

    override fun saveCredo(userCredo: UserCredo) {
        sharedPreferences.edit {
            putString(USER_NAME_KEY, userCredo.login)
            putString(USER_PASSWORD_KEY, userCredo.password)
        }
    }

    override fun getCredo(): UserCredo {
        val userName = sharedPreferences.getString(USER_NAME_KEY, EMPTY_STRING)
        val userPassword = sharedPreferences.getString(USER_PASSWORD_KEY, EMPTY_STRING)
        return UserCredo(login = userName!!, password = userPassword!!)
    }

    companion object {

        const val SHARED_NAME = "SHARED_NAME"
        const val USER_NAME_KEY = "USER_NAME_KEY"
        const val USER_PASSWORD_KEY = "USER_PASSWORD_KEY"

        const val EMPTY_STRING = ""
    }
}