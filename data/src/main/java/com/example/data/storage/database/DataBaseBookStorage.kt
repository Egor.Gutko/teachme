package com.example.data.storage.database

interface DataBaseBookStorage {

    suspend fun getBooks(): List<BookEntity>

    suspend fun insertBook(bookEntity: BookEntity)
}