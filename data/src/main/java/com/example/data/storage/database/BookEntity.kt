package com.example.data.storage.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.data.storage.database.BookEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class BookEntity(
    @PrimaryKey val id: Int? = null,
    @ColumnInfo(name = "title") val title: String? = null,
    @ColumnInfo(name = "author") val author: String? = null,
    @ColumnInfo(name = "genre") val genre: String? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "isbn") val isbn: String? = null,
    @ColumnInfo(name = "image") val image: String? = null,
    @ColumnInfo(name = "published") val published: String? = null,
    @ColumnInfo(name = "publisher") val publisher: String? = null
) {
    companion object {
        const val TABLE_NAME = "book_entity_table"
    }
}