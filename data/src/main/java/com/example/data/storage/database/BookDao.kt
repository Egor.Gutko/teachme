package com.example.data.storage.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface BookDao {

    @Query("SELECT * FROM book_entity_table")
    suspend fun getAllBooks(): List<BookEntity>

    @Insert(entity = BookEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllBooks(vararg bookEntity: BookEntity)
}