package com.example.data.repo

import com.example.data.storage.profile.ProfileStorage
import com.example.domain.models.UserProfileInfo
import com.example.domain.repository.UserProfileRepository

class UserProfileRepositoryImpl(private val profileStorage: ProfileStorage) :
    UserProfileRepository {

    override fun getUserProfileInfo(): UserProfileInfo {
        return profileStorage.getProfileInfo()
    }

    override fun editProfileNumber(number: String) {
        return profileStorage.editProfileNumber(number)
    }

    override fun editProfileName(name: String) {
        return profileStorage.editProfileName(name)
    }

    override fun editProfileAge(age: Int) {
        return profileStorage.editProfileAge(age)
    }
}