package com.example.data.repo

import com.example.data.storage.auth.AuthStorage
import com.example.domain.models.RegistrationUserModel
import com.example.domain.models.UserCredo
import com.example.domain.repository.AuthRepository

class AuthRepositoryImpl( private val authStorage: AuthStorage) : AuthRepository {

    override fun checkUserCredo(userCredo: UserCredo): Boolean {
        val user = authStorage.getCredo()
        return userCredo.login.isNotEmpty()
                && userCredo.password.isNotEmpty()
                && userCredo.login == user.login
                && userCredo.password == user.password
    }

    override fun registerUser(model: RegistrationUserModel): Boolean {
        return if (
            model.login.isNotEmpty()
            && model.password.isNotEmpty()
            && model.password == model.repeatPassword
        ) {
            authStorage.saveCredo(UserCredo(model.login, model.password))
            return true
        } else {
            return false
        }
    }
}